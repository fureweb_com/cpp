//namespace에 대한 이야기 3 - using
#include <iostream>

//using 키워드는
//1) using namespace namespace_object_name;
//2) using namespace_object_name::function_name or field_name;
//형태로 사용할 수 있다.
using std::cout;
using std::cin;
using std::endl;

namespace A_COM
{
	void function(void){
		cout << "A.com에서 정의한 함수" << endl;
	}
}

namespace B_COM
{
	void function(void){
		cout << "B.com에서 정의한 함수" << endl;
	}
}

using A_COM::function; //main function scope 내에서 function이라는 함수를 바로 호출할 수 있게 된다.

//이 위쪽 부분은 절차적으로 선언되어야 하는것 같다. std 네임스페이스를 위에서 쓴다고 해놓지 않으면, A_COM 아래 함수 선언에서 제대로 사용도 못한다.
int main(void)
{
	function();
	A_COM::function();
	B_COM::function();
	return 0;
}

