#include <iostream>
//함수 오버로딩

//C언에에서는 아래와 같은 함수 정의가 허용되지 않는다.
int function( void ) {
	return 10;
}

int function( int a, int b ) {
	return a + b;
}

//C언어에서는 함수 오버로딩이 허용되지 않았기 때문.
//한번 선언된 함수명은 완전히 고유했으며, 겹칠 수 없었다.
//즉, C 컴파일러는 함수를 호출할 때 그 이름만 가지고 호출을 하게 되어있는데
//C++ 컴파일러는 함수와 그 함수에서 사용해야하는 파라미터까지 함께 찾아보도록 되어있다.
//그래서 오버로딩이 허용되는 것이다.
//다른 언어들에서의 오버로딩도 이러한 형태로 구현되어있는 것 같음.
//즉, C++에서는 어떤 함수를 호출할 때 이름만으로 호출하지 않는다는 이야기.

int main( void ) {
	std::cout << function() << std::endl;
	std::cout << function( 12, 13 ) << std::endl;
	return 0;
}