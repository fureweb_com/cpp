//예전 스타일. 현재 컴파일러로는 컴파일 조차 되지 않음. iostream.h 헤더 자체를 인클루드 할 수 없게 된것같음.(fatal error: iostream.h: No such file or directory compilation terminated.)
#include <iostream.h>

int main(void){
	cout << " Hello World!! " << endl;
	cout << " Hello" << " World!! " << endl;
	cout << 1 << ' a ' << " String " << endl; // warning: multi-character character constant [-Wmultichar]
	return 0;
}