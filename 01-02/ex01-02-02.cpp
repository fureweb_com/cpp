//적절히 오버로딩된 함수가 호출되는 예제
#include <iostream>

void function( void ){
	std::cout << "function( void ) called" << std::endl;
}

void function( char c ){
	std::cout << "function( char c ) called" << std::endl;
}

void function( int a, int b ){
	std::cout << "function( int a, int b ) called" << std::endl;
}

int main( void ) {
	function();
	function( 'a' );
	function( 1, 2 );

	return 0;
}