#include <iostream>
//return type이 다른것은 오버로딩 조건에 해당되지 않음. 
//아래와 같이 코딩한다면 컴파일러가 ambigous하다는 오류를 던짐.

void function( void ){
	std::cout << "function( void ) called" << std::endl;
}

int function( void ){ //ambiguating new declaration of 'int function()' 이라는 컴파일 에러 발생
	std::cout << "int function( void ) called" << std::endl;
}

int main( void ) {
	function();
	return 0;
}