//Default parameter를 이용한 잘못된 오버로딩
#include <iostream>

using namespace std;

int function(int a = 10){
	return a + 1;
}

int function(void){
	return 10;
}

int main(void)
{
	//cout << function(10) << endl; //정상적으로 호출된다.

	//아래와 같은 함수 호출은 위 함수 정의 두가지 모두를 호출할 수 있다.
	//컴파일러는 위에서 정의된 함수 중 어떤것을 호출해야 할지 결정하지 못한다.
	//그러므로 컴파일 시 오류가 발생한다.
	cout << function() << endl; //error: call of overloaded 'function()' is ambiguous
	return 0;
}