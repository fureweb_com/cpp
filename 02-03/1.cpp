//레퍼런스의 이해
#include <iostream>

using namespace std;

int main()
{
	int val = 10;
	
	int *pVal = &val;
	int &rVal = val;

	cout << pVal << endl; //포인터 메모리 주소 출력
	cout << *pVal << endl; //포인터가 가리키는 메모리 주소에 들어있는 바이너리를 int형으로 변환한 값 출력
	cout << rVal << endl; //레퍼런스의 메모리 주소에 담겨있는 바이너리를 int형으로 변환한 값 출력
	cout << &rVal << endl; //참조변수의 메모리 주소 출력

	return 0;
}
