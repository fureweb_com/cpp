//default parameter
#include <iostream>

using namespace std;

int BoxVolume( int length, int width = 1, int height = 1 ); //뭐지 추상 메소드인가?
//책에서는 "이러한 형태의 구성은 이미 C언어를 통해서 학습한 것이다." 라면서 언급이 더이상 되어있지 않다. -_-

int main()
{
	cout << " [ 3, 3, 3 ]        : " << BoxVolume( 3, 3, 3 ) << endl;
	cout << " [ 5, 5, def ]      : " << BoxVolume( 5, 5 ) << endl;
	cout << " [ 7, def, def ]    : " << BoxVolume( 7 ) << endl;

	return 0;
}

int BoxVolume( int length, int width, int height )
{
	return length * width * height;
}