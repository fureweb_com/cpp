#include <iostream>
using namespace std;
//연습문제1-2 > 문제3
//다음 main함수에서 필요로 하는 swap 함수를 구현하라.
void swap(int *a, int *b) //이 swap 함수 호출 시 넘겨받는 변수 a와 b는 "포인터" 변수이다. main에서의 num1, num2 변수의 주소가 각각 담겨있다.
{
	int temp = *a; //swap 호출 당시 포인터 변수 a가 가리키던 주소에 담겨있던 값을 temp 변수에 저장시킨다.

	*a = *b; //포인터 변수 a가 확보하고 있는 메모리 주소에, 포인터 변수 b가 가리키고 있던 곳에 담겨있던 바이트를 그대로 들고와서 a에 할당시킨다.
	*b = temp; //포인터 변수 b가 확보하고있는 메모리 주소에 
}

void swap(char* a, char* b)
{
	cout << a << endl;
	cout << b << endl;
}

void swap(double* a, double* b)
{
	cout << a << endl;
	cout << b << endl;
}



int main()
{
	int num1=20, num2=30;
	cout << "swap 전 " << endl;
	std::cout<<&num1<<' '<<&num2<<std::endl;
	std::cout<<num1<<' '<<num2<<std::endl;

	swap(&num1, &num2); //함수 호출 시에는 &를 이용하여 참조형 변수의 메모리 주소를 넘기고, 메모리 주소를 받아 처리해야하는 함수 선언부에서는 *를 이용하여 그 주소를 받아 사용할 수 있다.
	cout << "swap 후 " << endl;
	std::cout<<&num1<<' '<<&num2<<std::endl;
	std::cout<<num1<<' '<<num2<<std::endl;

	// char ch1='a', ch2='z';
	// swap(&ch1, &ch2);
	// std::cout<<ch1<<' '<<ch2<<std::endl;

	// double dbl1=1.111, dbl2=5.555;
	// swap(&dbl1, &dbl2);
	// std::cout<<dbl1<<' '<<dbl2<<std::endl;

	return 0;
}

//요구되는 결과
//30 20
//z a
//5.555 1.111

