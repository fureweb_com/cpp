//default parameter
#include <iostream>

using namespace std;

//function에 기본값을 부여한다.(es6에서도 도입된 기능! / 타입은 기재 안해도 되지만.)
int function( int a = 0, int b = 10 ){
	return a + b + 1;
}

int main()
{
	cout << function( 11 ) << endl; //22
	cout << function() << endl; //11, function()이것의 의미는, function(0, 10)의 의미가 된다.

	return 0;
}