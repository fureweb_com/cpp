//bool
//C+++ 스타일의 프로그램
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

//c++에서는 primitive type으로 bool(ean)이 생겼고,
//true와 false라는 예약어가 생겼기때문에
//더이상 C 스타일처럼 TRUE / FALSE라는 상수를 선언하여 처리할 필요가 없다.
//하지만 C표준에 bool과 true와 false라는 예약어가 생겼기때문에
//2004년 이후 컴파일러라면 그냥 해당 예약어들을 사용할 수 있을 것이다.
//아마 요즘 컴파일러들로는 전부 처리가 가능할 것 같다.
//지금 보고있는 교재가 2004년꺼..
bool IsPostive(int i)
{
	if(i<0)
		return false;
	else
		return true;
}


int main(void)
{
	int num;
	bool result;

	cout << "input your number : ";
	cin >> num;

	result = IsPostive(num);

	if(result==true)
		printf("Positive number \n");
	else
		printf("Negative number \n");

	return 0;
}