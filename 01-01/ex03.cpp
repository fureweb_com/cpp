//이하 과거 iostream.h 파일 인클루드는 실행하지 않을것임.
#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
	int val1, val2;
	cout<<"첫 번째 숫자 입력 : ";
	cin>>val1;

	cout<<"두 번째 숫자 입력 : ";
	cin>>val2;

	int result = val1+val2;
	cout<<"덧셈 결과 : " <<result<<endl;

	return 0;
}