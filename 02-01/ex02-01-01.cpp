//bool
//C 스타일의 프로그램
#include <stdio.h>

const int TRUE = 1;
const int FALSE = 0;

int IsPostive(int i)
{
	if(i<0)
		return FALSE;
	else
		return TRUE;
}


int main(void)
{
	int num;
	int result;

	printf("input your number : ");
	scanf("%d", &num); //표준 입력 스트림을 통해 입력받은 값이 10진수이기때문에, 입력받은 숫자를 바이너리로 변환하여 num 변수의 메모리 주소에 그대로 꽂아버린다.

	result = IsPostive(num);

	if(result==TRUE)
		printf("Positive number \n");
	else
		printf("Negative number \n");

	return 0;
}