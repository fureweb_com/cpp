//연습문제1-2 > 문제3
//다음 main함수에서 필요로 하는 swap 함수를 구현하라.
int main()
{
	int num1=20, num2=30;
	swap(&num1, &num2);
	std::cout<<num1<<' '<<num2<<std::endl;

	char ch1='a', ch2='z';
	swap(&ch1, &ch2);
	std::cout<<ch1<<' '<<ch2<<std::endl;

	double dbl1=1.111, dbl2=5.555;
	swap(&dbl1, &dbl2);
	std::cout<<dbl1<<' '<<dbl2<<std::endl;

	return 0;
}

//요구되는 결과
//30 20
//z a
//5.555 1.111