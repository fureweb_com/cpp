//namespace에 대한 이야기
#include <iostream>

using namespace std;

namespace A_COM
{
	void function(void)
	{
		cout << "A.com에서 정의한 함수 " << endl;
	}
}

namespace B_COM
{
	void function(void)
	{
		cout << "B.com에서 정의한 함수 " << endl;
	}
}

// 아래 33번째 줄에서 호출할 수 있으려면 이곳에 이렇게 별도로 function을 선언해줘야한다. 다 다른 함수임.
// void function(void)
// {
// 	cout << "Global Scope에서 정의한 함수 " << endl;
// }

int main()
{

	A_COM::function();
	B_COM::function();
	//function(); //error: 'function' was not declared in this scope -> global scope에는 선언한 적이 없어서 실행안됨.
	return 0;
}