//사용자로부터 총 10개의 정수를 입력받아서 그 합을 출력하는 프로그램
#include <iostream>

using namespace std;

int main(void)
{
	int num, result;

	cout << "input 10 numbers below." << endl;
	for (int i = 0; i < 10; ++i)
	{
		cin >> num;
		result += num;
	}

	cout << "Total : " << result << endl;

	return 0;
}