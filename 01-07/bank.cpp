//bank account management program
//한글 깨짐은 windows cmd에서 chcp 65001로 UTF-8로 변경하여 일부 사용할 수도 있겠으나
//제대로 해결은 안됨. 
#include <iostream>
#include <string.h> //예제 코드에서 사용되지 않았던 라이브러리.. strcpy 라는 함수는 이곳에 들어있던 함수인듯하다.

using namespace std;
const int NAME_LEN = 20; //전역에서 사용할 상수

//구조체 정의
typedef struct _Account
{
	int id; //통장계좌
	int balance; //잔고
	char name[NAME_LEN]; //고객이름
} Account; //Class인 것 처럼, 바디 끝 우측에 기재된 단어를 타입으로 사용할 수 있군..


Account pArray[100]; //고객별 Account를 저장하기 위한 배열
int index = 0; //현재 저장된 Account 수

//사용할 함수 나열
void PrintMenu(); //전체 메뉴 출력
void MakeAccount(); //계좌 개설
void Deposit(); //입금
void Withdraw(); //출금
void Inquire(); //잔액조회

//enum???
enum{ MAKE=1, DEPOSIT, WITHDRAW, INQUIRE, EXIT };


//프로그램 시작
int main()
{
	int choice;

	while(1)
	{
		PrintMenu();
		cout << endl << "선택 : ";
		cin >> choice;

		switch(choice)
		{
			case MAKE:
				MakeAccount();
				break;
			case DEPOSIT:
				Deposit();
				break;
			case WITHDRAW:
				Withdraw();
				break;
			case INQUIRE:
				Inquire();
				break;
			case EXIT:
				return 0;
			default:
				cout << "잘못된 값을 선택했습니다. 다음에 제시된 값 중 하나를 선택해주세요." << endl;
				break;
		}
	}

	return 0;
}

void PrintMenu()
{
	cout << "-------- Menu --------" << endl;
	cout << "1. 계좌 개설" << endl;
	cout << "2. 입금" << endl;
	cout << "3. 출금" << endl;
	cout << "4. 잔액 조회" << endl;
	cout << "5. 프로그램 종료" << endl;
}

void MakeAccount()
{
	int id;
	char name[NAME_LEN];
	int balance;

	cout << "계좌 개설----------------" << endl;
	cout << "계좌 번호\t: "; cin >> id;
	cout << "이름    \t: "; cin >> name;
	cout << "잔고    \t: "; cin >> balance;

	pArray[index].id = id;
	pArray[index].balance = id;
	strcpy(pArray[index].name, name);

	index++; //index는 전역변수. 생성 후 parray 배열 내에 값들을 할당한 뒤 번호를 추가시킨다.
}

void Deposit()
{
	int money;
	int id;

	cout << "입금시킬 계좌번호\t: "; cin >> id;
	cout << "입금액        \t: "; cin >> money;

	for(int i = 0; i < index; i++)
	{
		if(pArray[i].id == id)
		{
			pArray[i].balance += money;
			cout << "입금 완료! 잔액은 " << pArray[i].balance << "원 입니다." << endl;
			return;
		}
	}
	cout << "유효하지 않은 계좌번호입니다." << endl;
}

void Withdraw()
{
	int money;
	int id;

	cout << "계좌번호      \t: "; cin >> id;
	cout << "입금액        \t: "; cin >> money;

	for(int i = 0; i < index; i++)
	{
		if(pArray[i].id == id)
		{
			if(pArray[i].balance < money)
			{
				cout << "잔액이 부족합니다." << endl;
				return;
			}
			pArray[i].balance -= money;
			cout << "출금 완료! 잔액은 " << pArray[i].balance << "원 입니다." << endl;
			return;
		}
	}
	cout << "유효하지 않은 계좌번호입니다." << endl;
}

void Inquire()
{
	for(int i = 0; i < index; i++)
	{
		cout << "계좌 번호\t: " << pArray[i].id << endl;
		cout << "이름    \t: " << pArray[i].name << endl;
		cout << "잔고    \t: " << pArray[i].balance << endl;
	}
}























