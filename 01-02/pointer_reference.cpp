#include <iostream>

using namespace std;

int main()
{

	//c계열에서의 값이란?
	//메모리 주소에 담겨있는 자료형으로 변환되어 보여줘야 하는 것.
	//자료형 char로 표현될 0000 0001은?
	//자료형 int로 표현될 0000 0001은?



	//reference에 대한 이야기
	//signed int는 4byte (32bit)
	int a = 1; //int형 변수 a 선언 후 1이라는 10진수 값 할당. 바이너리로는 -> (00000000 00000000 00000000 00000001)
	int &rep = a; //int형 레퍼런스 rep 선언 후 a 변수의 값을 할당. (값이라는 것은 결국 메모리의 주소)
	
	
	int *p = &a; //포인터 변수에는 선언 시 주소 자체를 할당하거나
	p = &a; //선언 후 p라는 변수가 가리키는 주소값에 a 변수의 주소값을 할당시킬 수 있다.
	//위 두 문장은 동일하다.
	//즉 포인터변수는 그 이름 자체가 주소값이며, &p = &a; 이런식으로 할당할 수는 없다.
	//왜냐하면 &p는 무언가를 담을 수 있는 그릇이 아니다.
	//또한 * 연산자는 변수를 선언 시에만 포인터 변수임을 표기해줄 수 있고,
	//그 외에서는 산술연산자 *로써 사용되므로 사용할 수 없다.
	//*p = &a; 이 문장은 invalid conversion from int* to int 라는 오류를 발생시킨다.
	//좌변의 포인터가 




	//void형 포인터 변수를 선언할 수도 있다.


	cout << "a value     : " << a << endl;
	cout << "a address   : " << &a << endl;
	cout << "rep value   : " << rep << endl;
	cout << "rep address : " << &rep << endl;
	cout << "p value     : " << *p << endl;
	cout << "p address   : " << p << endl;
	cout << "&p          : " << &p << endl;



	// int *p; //포인터 변수 선언

	// std::cout << "&p (0x72fe38) : " << &p << std::endl; //0x72fe38
	// std::cout << "p  (0x1a)     : " << p << std::endl; //0x1a

	return 0;
}