//int형과 bool형 사이에 값 대입
#include <iostream>

using std::cout;
using std::endl;

int main(void)
{
	int BOOL = true;
	cout << "BOOL : " << BOOL << endl;

	BOOL = false;
	cout << "BOOL : " << BOOL << endl;

	bool Bool = 1;
	cout << "Bool : " << Bool << endl;

	Bool = 0;
	cout << "Bool : " << Bool << endl;

	return 0;
}