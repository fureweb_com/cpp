//namespace에 대한 이야기 2
#include <iostream>

using namespace std;

//아래 namespace는 전처리기를 통해 이러한 함수가 있다는 것을 컴파일 하기 전에 처리시키는 것인가?
//만약 이러한 함수 선언을 했다면, 아래 쪽에서 실제로 구현을 시켜줘야만 컴파일 에러가 발생하지 않게됨.
//만약 구현하지 않고 main에서 호출하고 있다면 undefined reference to `A_COM::function()' 이런식으로 에러가 발생함.
namespace A_COM
{
	void function(void);
}

namespace B_COM
{
	void function(void);
}

int main(void)
{

	A_COM::function();
	B_COM::function();
	return 0;
}

namespace A_COM
{
	void function(void){
		cout << "A.com에서 정의한 함수" << endl;
	}
}

namespace B_COM
{
	void function(void){
		cout << "B.com에서 정의한 함수" << endl;
	}
}