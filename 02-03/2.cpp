//레퍼런스의 이해
#include <iostream>

using namespace std;

int main()
{
	int val = 10;
	int &ref = val; //ref = 10

	val++; //val = 11, ref = 11
	cout << "ref : " << ref << endl; //11
	cout << "val : " << val << endl; //11

	ref++; //결과적으로 val 변수의 주소를 가지고 있고, 똑같이 사용된다. 단지 alias일뿐.
	cout << "ref : " << ref << endl; //12
	cout << "val : " << val << endl; //12

	return 0;
}
