//판매원들의 급여 계산 프로그램을 작성해 보자.
//이 회사는 모든 판매원에게 매달 50만원의 기본 급여와
//물품 판매 가격의 12%에 해당하는 돈을 지급한다.
//민수가 이달 100만원을 팔았다면
//50+100*0.12 = 62
//62만원을 지급받게된다.
//100 -> 62만원
//200 -> 74만원

#include <iostream>

using namespace std;

int main(void)
{
	int number;

	cout << "input your total sales this month : ";
	cin >> number;

	if( number > 0 )
		number = 50 + (number * 0.12);
	else
	{
		cout << "Salaries this month : 0. sorry :(" << endl;
		return 0;
	}

	cout << "Salaries this month : " << number << endl;

	return 0;
}