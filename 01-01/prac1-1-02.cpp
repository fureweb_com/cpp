//사용자로부터 이름과 전화번호를 입력받아서 배열에 저장한 다음 그대로 출력해주는 프로그램
#include <iostream>

using namespace std;

int main(void)
{
	char name[100];
	char phone_number[100];

	cout << "input your name : ";
	cin >> name;

	cout << "input your phone number : ";
	cin >> phone_number;

	cout << endl;
	cout << "YOUR NAME         : " << name << endl;
	cout << "YOUR PHONE NUMBER : " << phone_number << endl;

	return 0;
}