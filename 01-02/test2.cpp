#include <iostream>

using namespace std;

int main()
{
	int a = 10;
	int & refa = a;

	cout << &a << endl;
	cout << refa << endl;

	int *p;

	cout << p << endl;
	p = &a;
	cout << p << endl;

	return 0;
}