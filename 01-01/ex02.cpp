//iostream.h 까지 include 시킨 경우, std 네임스페이스까지 자동으로 사용되어 std 내 멤버들을 그냥 사용할 수 있어 보인다.
//iostream만 include하는 경우(신문법), std 는 별도로 using 선언을 해줘야 하고, 아니면 컴파일 에러가 발생함.
#include <iostream>

int main(void){
	std::cout << " Hello World!! " << std::endl;
	std::cout << " Hello" << " World!! " << std::endl;
	std::cout << 1 << ' a ' << " String " << std::endl; // warning: multi-character character constant [-Wmultichar]
	return 0;
}