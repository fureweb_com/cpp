//숫자를 하나 입력받아서 그 숫자에 해당하는 구구단을 출력하는 프로그램.
//입력 숫자가 5였다면, 5단 전체를 추력해야한다.
#include <iostream>

using namespace std;

int main(void)
{
	int number;

	cout << "input your number : ";
	cin >> number;

	if( number < 2 || number > 9 )
	{
		cout << "please input number between 2 and 9" << endl;
		return 0;
	}

	for( int i = 1; i < 10; i++ )
	{
		cout << number << " * " << i << " = " << number * i << endl;
	}

	return 0;
}